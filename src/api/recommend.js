import jsonp from '../common/js/jsonp'
import axios from 'axios'
import {commonParam, options} from "./config";

export function getRecommend() {
  const url = 'https://c.y.qq.com/musichall/fcgi-bin/fcg_yqqhomepagerecommend.fcg';
  const data = Object.assign({}, commonParam, {
    uin: '1085077154',
    platform: 'h5',
    needNewCode: 1
  });
  return jsonp(url, data, options);
}

export function getDiscList() {
  const url = '/api/getDiscList';
  const data = Object.assign({}, commonParam, {
    platform: 'yqq',
    hostUin: '1085077154',
    needNewCode: 0,
    sin: 0,
    ein: 29,
    sortId: 5,
    rnd: Math.random(),
    categoryId: 10000000,
    format: 'json'
  });
  return axios.get(url, {
    params: data
  }).then((res) => {
    return Promise.resolve(res.data);
  });
}
